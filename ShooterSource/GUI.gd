extends CanvasLayer

var is_paused = false
var pause_delay = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$Pause_Menu.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(Input.is_action_just_pressed("ui_pause") and pause_delay == false):
		toggle_pause()
		
func toggle_pause():
	if (is_paused == true):
		is_paused = false
		$Pause_Menu.visible = false
		get_tree().paused = false
	else:
		is_paused = true
		$Pause_Menu.visible = true			
		get_tree().paused = true


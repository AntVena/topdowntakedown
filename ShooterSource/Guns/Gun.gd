extends Node2D

export var has_tracer : bool	#does the gun have tracers?
export var bullet_speed : int 
export var tracer_offset : int	#how many rounds between tracers?
export var spread : int
export var min_damage : int
export var max_damage : int
export var has_flashlight 	= 	true	#does this gun have a flashlight at all?
export var is_silenced 		=	false
var in_firing_delay			= 	false	#Are we currently waiting between bullets?
var tracer_countdown		= 0		#how many bullets have fired since last tracer
onready var spread_range = Vector2(-spread, spread)#amount of spread, random between x & y
onready var rate_of_fire = get_node("rate_of_fire") #todo: change to an export var and set timer in code

var Bullet = preload("res://Bullets/Bullets.tscn") #the default bullet


# Called when the node enters the scene tree for the first time.
func _ready():
	$muzzle_flash/muzzle_flash_sprite.visible = false
	print(spread)
	print(spread_range)
func fire():
	#set per-shot gun variables
	in_firing_delay = true
	rate_of_fire.start()
	tracer_countdown = tracer_countdown + 1;
	var randomized_spread = Vector2(rand_range(spread_range.x,spread_range.y),rand_range(spread_range.x,spread_range.y))
	
	#create a bullet
	var bullet = Bullet.instance()
	var spawn_position = $bullet_emitter.get_global_position()
	var guide_position = $bullet_emitter/bullet_guide.get_global_position()
	bullet.shooter = get_parent()
	bullet.damage = rand_range(min_damage, max_damage)
	if(has_tracer and tracer_countdown % tracer_offset == 0):
		bullet.is_tracer = true
		tracer_countdown = 0;
	
	#stop the bullet from colliding wih the firer
	bullet.add_collision_exception_with(bullet.shooter)
	
	#add the bullet to the scene
	get_parent().get_parent().add_child(bullet)
	bullet.set_global_position(spawn_position)
	bullet.linear_velocity = (((guide_position + randomized_spread) - bullet.get_global_position()).normalized()) * bullet_speed
	bullet.look_at(guide_position)
	
	#work that movie magic
	$weapon_animations.play("muzzle_flash")
	$muzzle_flash/muzzle_flash_sprite.frame = rand_range(1,9) #1-[number of possible muzzle flash frames]
	
	#if the gun is not silenced, emit a sound
	if(is_silenced != true):
		for body in $sound.get_overlapping_bodies():
			if (body.is_in_group("Enemies")): #update to use groups later
				body.heard_noise_at = get_global_position()
				
func _on_rate_of_fire_timeout():
	in_firing_delay = false

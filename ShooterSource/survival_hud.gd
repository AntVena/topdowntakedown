extends Control

#The round time stuff is handled by the GUI because it counts down visibly. 
#This will bite me later mark my words
signal timer_expired

var seconds: int = 45
var minutes: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$enemy_kill_count.text = "Enemies Killed"
	$time_countdown.text = String(minutes) + ":" + String(seconds)
	$seconds_timer.start(1)

func _on_seconds_timer_timeout():
	
	#if there's time left at all
	if(seconds > 0 or minutes > 0):
		seconds -= 1
		
		#if there are minutes left count one down when seconds hit 0
		if (seconds < 0 and minutes > 0):
			minutes -= 1
			seconds = 59
	
	#if there's no time left, signal the timeout
	else:
		emit_signal("timer_expired")
	
	$time_countdown.text = String(minutes) + ":" + "%02d" % seconds
	$seconds_timer.start()

func update_timer(new_seconds, new_minutes):
	seconds = new_seconds
	minutes = new_minutes

func update_enemy_counter(enemies_killed, enemies_total):
	$enemy_kill_count/enemies_killed.text = String(enemies_killed)
	$enemy_kill_count/enemies_total.text = String(enemies_total)
	

func _on_Survival_Mode_round_start(current_round):
	#reset the enemy kill counter
	update_timer(45, 0)
	announce_round(current_round)
	

func announce_round(current_round):
	#set up the fancy round banner and fade it out
	$round_counter.text = ("Round " + String(current_round))
	$round_counter.visible = true
	$round_counter.modulate.a = 1.0
	$Tween.interpolate_property($round_counter,"modulate:a",1.0 , 0.0, 3, $Tween.TRANS_LINEAR, $Tween.EASE_IN)
	$Tween.start()
	yield($Tween,"tween_completed")
	$round_counter.visible = false
	
func _on_Survival_Mode_wave_start(current_wave):
	$wave_counter/wave_number.text = String(current_wave)
	
func _on_Survival_Mode_wave_completed(_current_wave):
	#for potential payouts at wave ends
	pass

func _on_Survival_Mode_round_completed(_current_round):
	#ToDo: implement a wait timer between rounds for shopping
	pass

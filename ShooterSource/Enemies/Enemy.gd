extends KinematicBody2D

signal died()

export var attack_damage = 5
export var speed = 50
export var health = 100
var dead = false
var tracking_target = false
var target
var target_in_range = false
var heard_noise_at = Vector2(0,0)
var respawns = false
var attacking = false
var factor_count = 0
onready var course = get_global_position()
onready var anim = get_node("Animator")

# Called when the node enters the scene tree for the first time.
func _ready():
	$body.visible = true				#make sure we can see it
	$body/skin.modulate = Color(1,1,1)	#make sure it's the right colour
	$look_around_timer.start(rand_range(0.5,1.0))
	
	
###############START_MAIN_LOOP########################################
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):

	if (not dead): 
		#eventual movement destination is divided by factors to get average destination
		factor_count = 0
		course = self.get_global_position()
		for body in $personal_space.get_overlapping_bodies():
			if (body.is_in_group("Enemies") and body != self):
				course += body.get_global_position().direction_to(position)
		
		#if tracking handle moving toward targets or attacking
		if (tracking_target): 
			var target_position = target.get_global_position()
			if (not is_at(target_position)):
				self.look_at(target_position)
				course += position.direction_to(target_position)
				
				#they just saw someone they dont care about where they heard the last sound anymore
				if(heard_noise_at != Vector2(0,0)):
					heard_noise_at =Vector2(0,0)
			else:
				melee_attack_target(target)
			
		#If idle, listen for noises in range and investigate them
		else:
			if(heard_noise_at != Vector2(0,0)):
				if(not is_at(heard_noise_at)):
					self.look_at(heard_noise_at)
					course += position.direction_to(heard_noise_at)
					
		#now if we want to move, then move!	
		if (course != self.position):
			move_and_slide(walk_to(course))
#################END_MAIN_LOOP#################################

func is_at(target_position):
	if(self.get_global_position().distance_to(target_position) < 40):
		return true
	else:
		return false

func is_near(target_position):
	if(self.get_global_position().distance_to(target_position) < 80):
		return true
	else:
		return false
	
func alter_course(current_direction, target_position):
		var direction = current_direction.direction_to(target_position)
		return direction
	
func walk_to(target_direction):
#face the target and play the walking animation
	if not anim.is_playing():
		anim.play("walking")
	
	#move
	var movement = self.position.direction_to(target_direction) * (speed)
	return movement
	
func melee_attack_target(body):	
		var attack_animation = "attacking" + String(int(rand_range(1,2)))
		self.look_at(body.position)
		if not anim.is_playing():
			anim.play(attack_animation)
			target.take_damage(rand_range(1,attack_damage))
		
func take_damage(damage:int):
	health = health-damage

	#handle different amounts of health
	if(health > 50):
		$Animator.play("take_damage_healthy")
	elif(health < 50 and health > 25):
		$Animator.play("take_damage_hurt")
	elif(health < 25 and health > 0):
		$Animator.play("take_damage_dying")
	elif(health < 1):
		call_deferred("die")
	
	if (not $blood_splatter.emitting):
		$blood_splatter.emitting = true
	
func die():
	#randomly select a death animation and then die
	remove_from_group("Enemies")
	var death_animation = "die" + String(int(rand_range(1,5)))
	$Animator.play(death_animation)
	$body.disabled = true
	$body/shadow.queue_free()
	self.z_index = -10
	dead = true;
	emit_signal("died")
	
func scan_for(body):
	if(body != null): 
		if(body.get_name() == "Player"):
			var god_eye = get_world_2d().direct_space_state
			var intersections = god_eye.intersect_ray(get_global_position(), body.get_global_position(), [self], 15)
			if(intersections.has("collider")):
				return intersections.collider.name
			else:
				return false
		else:
			#todo: remove this later
			print(target)
			$Line2D.set_point_position(0, Vector2(0,0))
			$Line2D.set_point_position(1, to_local(target.position))
		return true

func _on_line_of_sight_body_entered(body):
		if(body.is_in_group("Survivors")):
			target_in_range = true
			#target = body
			if(scan_for(body) == "Player"):
				tracking_target = true
				target = body

func _on_line_of_sight_body_exited(body):
	if(body.get_name() == "Player"):
			target_in_range = false
			tracking_target = false

func _on_look_around_timer_timeout():
	for body in $line_of_sight.get_overlapping_bodies():
		if body.is_in_group("Survivors"):
			if( scan_for(body)):
				if( scan_for(body) == "Player" ): 
					tracking_target = true
					target = body
	$look_around_timer.start(rand_range(0.5,1.0))

	
	

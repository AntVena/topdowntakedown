extends Node2D

var loot

var Yellow = preload("res://Guns/Yellow.tscn")
var M16 = preload("res://Guns/Yellow.tscn")

func _on_Area2D_body_entered(body):
	if (body.get_name() == "Player"):
		
		#get the item with the same name as us
		match get_name():
			"Yellow":
				loot = Yellow.instance()

			"M16":
				print("M16")
			"Medkit":
				print("")
		#replace with what we actually want to do with it later
		if loot:
			body.equip_weapon(loot)
		queue_free()


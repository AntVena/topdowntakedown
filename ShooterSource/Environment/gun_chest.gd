extends Sprite

func _ready():
	$Item.visible = false
	frame = 0

func _on_open_area_body_entered(body):
	if(body.get_name() == "Player"):
		$anim.play("open")


func _on_open_area_body_exited(body):
	if(body.get_name() == "Player"):
		$anim.play("close")

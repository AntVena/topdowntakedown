extends Position2D

signal enemy_spawned

var Walker = preload("res://Enemies/Enemy.tscn")
export var spawning_enabled : bool
	
# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("Spawners")
	self.connect("enemy_spawned", find_parent("Survival_Mode"), "_on_Spawner_Enemy_spawned")

func _on_Timer_timeout():
	if (spawning_enabled):
		attempt_spawn_walker()

func attempt_spawn_walker():

	#if the randomly selected position is colliding with something, move
	if($collision_area.get_overlapping_bodies().size()>0):
		return false
	else:
		#spawn a zombie
		if(get_child_count() < 16):
			var walker = Walker.instance()
			get_parent().add_child(walker)
			walker.position = $collision_area.get_global_position()
			walker.add_to_group("Enemies")
			emit_signal("enemy_spawned")
			walker.connect("died", find_parent("Survival_Mode"), "_on_Enemy_died")
			


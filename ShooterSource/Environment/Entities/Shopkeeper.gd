extends Sprite

var is_open : bool 

func _ready():
	self.frame = 0
	is_open = false
	
func _on_survival_hud_timer_expired():
	if is_open == true:
		is_open == false
		print("Shopkeeper is closing shop")
		$AnimationPlayer.play("close")

func _on_Survival_Mode_round_completed():
	is_open = true
	$AnimationPlayer.play("open")

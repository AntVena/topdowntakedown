extends Node2D

signal round_start
signal round_completed
signal wave_start
signal wave_completed

var between_rounds: bool = false
var current_wave : int = 1
var waves_in_round : int = 5
var current_round : int = 1
var enemies_killed : int = 0
var enemies_total: int = 0
onready var survival_hud = find_node("survival_hud")

# Called when the node enters the scene tree for the first time.
func _ready():
	round_start()
		
func _on_Enemy_died():
	enemies_killed += 1
	survival_hud.update_enemy_counter(enemies_killed, enemies_total)
	if(enemies_killed >= enemies_total):
		wave_completed()
		
	
func round_completed():
	current_round += 1
	emit_signal("round_completed")
	survival_hud.update_timer(30, 0)
	between_rounds = true
	print("Shop is open")

func round_start():
	#reset the enemy kill counter
	enemies_killed = 0
	current_wave = 0
	wave_start()
	emit_signal("round_start", current_round)

func wave_start():
	current_wave += 1
	enemies_killed = 0
	spawn_wave()
	update_enemy_count()
	survival_hud.update_timer(45, 0)
	emit_signal("wave_start", current_wave)

func wave_completed():
	emit_signal("wave_completed", current_wave)
	if (current_wave >= waves_in_round):
		round_completed()
	else:
		wave_start()
		
func spawn_wave():
	get_tree().call_group("Spawners","attempt_spawn_walker")

func update_enemy_count():
	enemies_total = get_tree().get_nodes_in_group("Enemies").size()
	survival_hud.update_enemy_counter(enemies_killed, enemies_total)
	return enemies_total

func _on_Spawner_Enemy_spawned():
	update_enemy_count()


func _on_survival_hud_timer_expired():
	if between_rounds == true:
		round_start()
		between_rounds = false
		
	elif current_wave < waves_in_round:
		wave_start()

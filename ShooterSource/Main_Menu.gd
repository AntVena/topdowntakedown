extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_survival_mode_button_pressed():
	get_tree().change_scene("res://Environment/Survival_Mode.tscn")


func _on_horde_tester_button_pressed():
	get_tree().change_scene("res://Environment/Horde_Tester.tscn")

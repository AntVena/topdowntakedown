extends RigidBody2D

var shooter #who shot it?
var is_tracer = false #is this a tracer round?
var damage : int = 1
const Enemy = preload("res://Enemies/Enemy.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	$impact_sparks.emitting = false
	
	($Timer as Timer).start()
	if (is_tracer):
		$bullet_body/trace_coating.enabled = true
		($Anim as AnimationPlayer).play("tracer_fade")

#remove the bullet from the game
func die():
	self.queue_free()
	
#make it vanish after a bit so it wont cause lag
func _on_Timer_timeout():
	die()


func _on_Bullet_body_entered(body):
	if(body.is_in_group("Enemies")):
		die() 	#the bullet hit something squishy so it should vanish
		body.take_damage(damage)

	else:
		#send it off in style
		call_deferred("hard_impact")

func hard_impact():
		self.linear_velocity = Vector2(0,0)
		self.angular_velocity = 0
		$bullet_body.disabled = true
		$bullet_body.visible = false
		$impact_sparks.visible = true
		$impact_sparks.emitting = true

extends KinematicBody2D

#signal health_changed(health)

var Bullet = preload("res://Bullets/Bullets.tscn")
var moving_right 			= false
var moving_left  			= false
var moving_down  			= false
var moving_up 				= false
var flashlight_is_on		= true #todo: move to gun later
var is_shooting				= false
var aiming_weapon			= false
export var aggro 			= 5
export var speed 			= 200
export var health:int 		= 100
export var max_health:int 	= 100

onready var weapon_animations = get_node("Gun/weapon_animations")
onready var	Gun = get_node("Gun")	


#non-global things that happen once when the node enters the scene
func _ready():
	weapon_animations.play("flashlight_on")
	set_health(max_health)

func _input(_event):
	moving_right = 	Input.is_action_pressed("move_right") 
	moving_left =	Input.is_action_pressed("move_left")
	moving_down =	Input.is_action_pressed("move_down") 
	moving_up 	=	Input.is_action_pressed("move_up")
	
	if(Input.is_action_pressed("aiming_weapon")):
		look_at(get_global_mouse_position())
		aiming_weapon = true
	else:
		aiming_weapon = false
		
	if(Input.is_action_pressed("toggle_flashlight")):
		toggle_flashlight()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	
	#set frame movement variables
	var movement = Vector2(0,0)

	movement = handle_movement()
	
	#handle gun useage (movement if for potential recoil)
	if(Input.is_action_pressed("fire") and not Gun.in_firing_delay):
		call_deferred("fire_gun")
	
	#apply movement values	
	if(movement != Vector2(0,0)):
		move_and_slide(movement)
	
	if (is_shooting):
		look_at(get_global_mouse_position())
		
#handle player-controlled movements
func handle_movement():
	var movement = Vector2(0,0)
	if(moving_left and not moving_right):
		movement.x -= 10
	if(moving_right and not moving_left):
		movement.x += 10
	if(moving_up and not moving_down):
		movement.y -= 10
	if(moving_down and not moving_up):
		movement.y += 10
	movement = movement.normalized() * speed
	if(movement != Vector2(0,0)):
		
		#if not shooting look ahead
		if(is_shooting == false and aiming_weapon == false):
			look_at(movement * 1000)

	return movement

func toggle_flashlight():
	if(!weapon_animations.is_playing()):
		if(flashlight_is_on == false):
			weapon_animations.play("flashlight_on")
			flashlight_is_on = true
		else:
			weapon_animations.play("flashlight_off")
			flashlight_is_on = false
		
func fire_gun():
	is_shooting = true
	look_at(get_global_mouse_position())
	#fire the gun
	Gun.fire()
	
	#make sure the character can aim freely while walking
	$aim_timer.start(5)

func take_damage(damage:int):
	health = health-damage
	if(health < 0):
		set_health(0)
	else:
		set_health(health)
		
func heal_damage(heal:int):
	health = health+heal
	if(health > max_health):
		set_health(max_health)
	else:
		 set_health(heal)
	
func set_health(value):
	health = value
	$HUD/health/value.text = String(health)
	$HUD/health/life_bar.value = health
	
func equip_weapon(gun):
	gun.name = Gun.name
	gun.rotation = Gun.rotation
	gun.position = Gun.position
	gun.z_index = 1
	remove_child(Gun)
	Gun.queue_free()
	call_deferred("add_child",gun)
	Gun = gun
	weapon_animations = gun.get_node("weapon_animations")




func _on_aim_timer_timeout():
	is_shooting = false

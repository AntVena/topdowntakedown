shader_type canvas_item;

//define wind speeds (can be negative, e.g negative east == west)
uniform float wind_speed_north =  0.01; 
uniform float wind_speed_east =  -0.005; 

//unless you specify otherwise fragment isa pplied to every pixel at once
void fragment(){
	//define how far each pixel of fog drifts 
	float east_drift = UV.x + (TIME * wind_speed_east);
	float north_drift = UV.y + (TIME * wind_speed_north);
	
	//calculate how far pixels moves north and east 
	vec4 fog_drift = texture(TEXTURE, vec2(east_drift, north_drift));
	
	//fragment is called on each pixel, so finally we apply the color
	//of the pixel to its new position
	COLOR = fog_drift;
}
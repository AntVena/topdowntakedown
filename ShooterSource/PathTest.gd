extends Sprite

#boilerplate related to signals I already made and am too lazy to remove
var heard_noise_at
var speed = 50
var path = PoolVector2Array()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			var player_position = get_parent().get_parent().get_parent().find_node("Player").position
			var path = get_parent().get_parent().get_parent().find_node("Navigation2D").get_simple_path(self.position, player_position)
			$Line2D.points = path

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Calculate the movement distance for this frame
	var distance_to_walk = speed * delta
	# Move the player along the path until he has run out of movement or the path ends.
	while path.size() > 0:
		print(path)
		var distance_to_next_point = position.distance_to(path[0])
		if distance_to_walk <= distance_to_next_point:
			# The player does not have enough movement left to get to the next point.
			position += position.direction_to(path[0]) * distance_to_walk
		else:
			print("shit")
			# The player get to the next point
			position = path[0]
			path.remove(0)
		# Update the distance to walk
		distance_to_walk -= distance_to_next_point
